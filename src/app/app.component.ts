import { Component } from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  angForm = new FormGroup({
    name : new FormControl("",Validators.required),
    address : new FormControl("",Validators.required)
  })

  get name(){
    return this.angForm.get('name')
  }

  get address(){
    return this.angForm.get('address')
  }


}
